<center>
<img src="https://camo.githubusercontent.com/23937f8930d508b1902dc7c654466a9228666f31/687474703a2f2f73372e7069636f66696c652e636f6d2f66696c652f383337383239353535302f312e706e67">
<center/>
  
# MyConfig V1
 [![GitHub license](https://img.shields.io/github/license/persepolisdm/persepolis.svg)](https://github.com/Yoord1992/MyConfig/blob/master/LICENSE)  [![Twitter Follow](https://img.shields.io/twitter/follow/persepolisdm.svg?style=social&label=Follow)](https://twitter.com/yoord1992)
 <p>
Auto Install Apps, Configs, ... On Your Debian Base Distro.
<p>

  &nbsp;

<b>How To Install Script </b>

1 - git clone https://github.com/Yoord1992/MyConfig

2 - cd MyConfig

3 - chmod +x ./start.sh

4 - ./start.sh  
&nbsp;
  <p>
    <p>
&nbsp;
&nbsp;
<b>Telegram :</b> Yoord
<p>
